if [[ -h "${HOME}/.zshenv" ]]; then # is symlink?
    zmodload -F zsh/stat b:zstat
    ZSHENV_LINK="$(zstat -L +link ${HOME}/.zshenv)"
    if [[ "${ZSHENV_LINK:0:1}" != "/" ]]; then # this is relative path
        ZSHENV_LINK="${HOME}/${ZSHENV_LINK}"
    fi
    ZDOTDIR="$(dirname $ZSHENV_LINK)"
elif [[ -f ${HOME}/.zshenv ]]; then
    ZDOTDIR="$HOME"
fi
export ZDOTDIR
