## http://zanshin.net/2013/02/02/zsh-configuration-from-the-ground-up/

# -------------------------------------------------------------------
# use nocorrect alias to prevent auto correct from "fixing" these
# -------------------------------------------------------------------
alias pip='nocorrect pip'
alias git='nocorrect git'
alias hg='nocorrect hg'

# Super user
alias pls='sudo'
alias please='sudo'
alias fuck='sudo $(fc -ln -1)'

# -------------------------------------------------------------------
# directory movement
# -------------------------------------------------------------------
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias cd..='cd ..'
alias cd...='cd ../..'
alias cd....='cd ../../..'
alias cd.....='cd ../../../..'
alias cd/='cd /'

alias 1='cd -'
alias 2='cd -2'
alias 3='cd -3'
alias 4='cd -4'
alias 5='cd -5'
alias 6='cd -6'
alias 7='cd -7'
alias 8='cd -8'
alias 9='cd -9'
alias 'bk=cd $OLDPWD'

# -------------------------------------------------------------------
# directory information
# -------------------------------------------------------------------

if is_mac || is_freebsd; then
    alias ls='ls -GF' # Colorize output, add file type indicator
elif is_linux; then
    alias ls='ls -F --color=auto' # Colorize output, add file type indicator
fi
alias l='ls -hl'
alias ll='ls -ahl'
alias lh='ls -d .*' # show hidden files/directories only
alias tree="ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/'"
alias 'dus=du -sckx * | sort -nr' #directories sorted by size

alias grep='grep --color=auto --exclude-dir=.cvs --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn'

alias md='mkdir -p'
alias rd=rmdir
alias d='dirs -v | head -10'

alias 'wordy=wc -w * | sort | tail -n10' # sort files in current directory by the number of words they contain
alias 'filecount=find . -type f | wc -l' # number of files (not directories)

# -------------------------------------------------------------------
# Mercurial (hg)
# -------------------------------------------------------------------
alias 'h=hg'
alias 'hs=hg status'
alias 'hc=hg commit'
alias 'hp=hg push'
alias 'hd=hg diff'
alias 'hm=hg commit -m'
alias 'hb=hg branch'
alias 'hl=hg log'
alias 'hpu=hg pull'
alias 'hcl=hg clone'
alias 'gta=hg tag -m'

# -------------------------------------------------------------------
# Git
# -------------------------------------------------------------------
alias g='git'
alias ga='git add'
alias gp='git push'
alias gpa='git push --all'
alias gl='git log'
alias gpl="git log --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias gs='git status'
alias gd='git diff'
alias gm='git commit -m'
alias gma='git commit -am'
alias gb='git branch'
alias gc='git checkout'
alias gcb='git checkout -b'
alias gra='git remote add'
alias grr='git remote rm'
alias gpu='git pull'
alias gcl='git clone'
alias gta='git tag -a -m'
alias gf='git reflog'
alias gv='git log --pretty=format:'%s' | cut -d " " -f 1 | sort | uniq -c | sort -nr'
alias gha="git hist --all"
alias gsm="git submodule"
alias gsms="git submodule status"
alias gsmu="git submodule update"
alias gsmur="git submodule update --remote"

# leverage aliases from ~/.gitconfig
alias gh='git hist'
alias gt='git today'

# curiosities 
# gsh shows the number of commits for the current repos for all developers
alias gsh="git shortlog | grep -E '^[ ]+\w+' | wc -l"

# gu shows a list of all developers and the number of commits they've made
alias gu="git shortlog | grep -E '^[^ ]'"

# -------------------------------------------------------------------
# Python virtualenv 
# -------------------------------------------------------------------
#alias mkenv='mkvirtualenv'
#alias on="workon"
#alias off="deactivate"

# -------------------------------------------------------------------
# Brew 
# -------------------------------------------------------------------
if is_mac && has_brew; then
    alias brews='brew list -1'
    alias bubu="brew update && brew upgrade && brew cleanup"
fi

# -------------------------------------------------------------------
# Vagrant 
# -------------------------------------------------------------------
if is_mac && has_vagrant; then
    alias vup='vagrant up'
    alias vd="vagrant destroy"
    alias vdf="vagrant destroy --force"
    alias vssh="vagrant ssh"
fi

alias vi="vim"
alias m="mvim"
alias 'ttop=top -ocpu -R -F -s 2 -n30' # fancy top

# alias to cat this file to display
alias acat="< ${ZSH_INCLUDES}/aliases.zsh"
alias fcat="< ${ZSH_INCLUDES}/functions.zsh"
alias sz="source ~/.zshrc" 

# Adding wget alias
alias wget="curl -O"


# -------------------------------------------------------------------
# Source: http://aur.archlinux.org/packages/lolbash/lolbash/lolbash.sh
# -------------------------------------------------------------------
alias wtf='dmesg'
alias onoz='cat /var/log/nginx/errors.log'
alias rtfm='man'
alias visible='echo'
alias invisible='cat'
alias moar='more'
alias icanhas='mkdir'
alias donotwant='rm'
alias dowant='cp'
alias gtfo='mv'
alias hai='cd'
alias plz='pwd'
alias inur='locate'
alias nomz='ps aux | less'
alias nomnom='killall'
alias cya='reboot'
alias kthxbai='halt'

if has_colordiff; then
    alias diff='colordiff'
fi

if has_mtr; then
    alias mtr='sudo mtr'
fi

if has_taskwarrior; then
    alias t='task'
    alias ta='task add'
    alias tb='task burndown'
    alias tc='task active'
    alias tg='task ghistory'
    alias tl='task next limit:10'
    alias ts='task summary'
    alias tw='task waiting'
    alias tu='task undo'
    alias tbd='task burndown.daily'
    alias tbm='task burndown.monthly'
    alias tbw='task burndown.weekly'
fi

if has_timewarrior; then
    alias ti='timew'
    alias tid='timew day'
    alias tig='timew gaps'
    alias tim='timew month'
    alias tis='timew summary :ids'
    alias tiw='timew week'
    alias tisw='timew summary :ids :week'
fi

if has_watson; then
    alias wa='watson'
    alias we='watson edit'
    alias wl='watson log'
    alias wld='watson log --day'
    alias wlw='watson log --week'
    alias wlm='watson log --month'
    alias wr='watson report'
    alias wrd='watson report --day'
    alias wrw='watson report --week'
    alias wrm='watson report --month'
    alias wp='watson stop'
    alias ws='watson status'
    alias wst='watson start'
fi
