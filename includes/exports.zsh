export PATH=${HOME}/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin

export LANG="en_US.UTF-8"
# Fix for mosh
export LC_CTYPE="en_US.UTF-8"
export WORDCHARS=''

# Set default console Java to 1.6
#export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/1.6/Home

# Enable color in grep
export GREP_COLOR='3;33'

# This resolves issues install the mysql, postgres, and other gems with native non universal binary extensions
export ARCHFLAGS='-arch x86_64'

export LESS='--ignore-case --raw-control-chars'
export PAGER='less'
export EDITOR='vim'
export GIT_PAGER='cat'

# CTAGS Sorting in VIM/Emacs is better behaved with this in place
export LC_COLLATE=C 

if has_go && [[ -d $HOME/go ]]; then
    export GOPATH=${HOME}/go
    export PATH=$GOPATH/bin:$PATH
fi

#export EC2_HOME=$HOME/soft/ec2-api-tools-1.6.8.0
#export PATH=/usr/local/bin:/usr/local/sbin:$PATH:$HOME/node_modules/.bin:$EC2_HOME/bin
#export NODE_PATH="/usr/local/lib/jsctags:/usr/local/lib/node_modules:$HOME/node_modules/"
#export PGDATA="/usr/local/var/postgres"
# Use gcc 4.2 instead of gcc 4
#export CC="/usr/bin/gcc-4.2"
#export JAVA_HOME=/usr
#export PYTHONPATH=$HOME/python_libs
#export NODE_PATH=/opt/github/homebrew/lib/node_modules
#export PYTHONPATH=/usr/local/lib/python2.6/site-packages

