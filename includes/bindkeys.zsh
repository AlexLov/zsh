# To see the key combo you want to use just do:
# cat > /dev/null
# And press it

# bindkey "^K"      kill-whole-line                      # ctrl-k
# bindkey "^R"      history-incremental-search-backward  # ctrl-r
# bindkey "^A"      beginning-of-line                    # ctrl-a  
# bindkey "^E"      end-of-line                          # ctrl-e
# bindkey "^D"      delete-char                          # ctrl-d
# bindkey "^F"      forward-char                         # ctrl-f
# bindkey "^B"      backward-char                        # ctrl-b

bindkey -e   # Default to standard emacs bindings, regardless of editor string

bindkey "\e="      copy-prev-shell-word                 # [Esc-=] - copy and paste prev word in line
bindkey -s '\el' 'ls -l\n'                              # [Esc-l] - run command: ls -l
has_git && bindkey -s '\eg' 'git status\n'              # [Esc-g] - run command: git status
has_hg && bindkey -s '\eh' 'hg status\n'                # [Esc-h] - run command: hg status
bindkey -s '\ej' 'fg\n'                                 # [Esc-j] - run command: fg
bindkey -s '\ed' 'date\n'                               # [Esc-d] - run command: date
bindkey ' ' magic-space                                 # [Space] - do history expansion
bindkey "^[[A"      up-line-or-search                   # start typing + [Up-Arrow] - fuzzy find history forward
bindkey "^[[B"      down-line-or-search                 # start typing + [Down-Arrow] - fuzzy find history backward
bindkey '^?' backward-delete-char                       # [Backspace] - delete backward

if [[ "${terminfo[kdch1]}" != "" ]]; then
    bindkey "${terminfo[kdch1]}" delete-char            # [Delete] - delete forward
else
    bindkey "^[[3~" delete-char
    bindkey "^[3;5~" delete-char
    bindkey "\e[3~" delete-char
fi

if [[ "${terminfo[kcbt]}" != "" ]]; then
    bindkey "${terminfo[kcbt]}" reverse-menu-complete   # [Shift-Tab] - move through the completion menu backwards
fi

if [[ "${terminfo[khome]}" != "" ]]; then
    bindkey "${terminfo[khome]}" beginning-of-line      # [Home] - Go to beginning of line
fi
if [[ "${terminfo[kend]}" != "" ]]; then
    bindkey "${terminfo[kend]}"  end-of-line            # [End] - Go to end of line
fi
