autoload -Uz add-zsh-hook vcs_info
setopt prompt_subst


## TODO: replace special icons to simple chars if not supported by terminal
(( MAXPWD = (${COLUMNS} - 1) / 4 ))
GIT_ICON=""
HG_ICON="☿"

# ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
# ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[yellow]%} ☂" # Ⓓ
# ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[cyan]%} ✭" # ⓣ
# ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%} ☀" # Ⓞ

# ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[cyan]%} ✚" # ⓐ ⑃
# ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[yellow]%} ⚡"  # ⓜ ⑁
# ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%} ✖" # ⓧ ⑂
# ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%} ➜" # ⓡ ⑄
# ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[magenta]%} ♒" # ⓤ ⑊
# ZSH_THEME_GIT_PROMPT_AHEAD="%{$fg[blue]%} 𝝙"
# More symbols to choose from:
# ☀ ✹ ☄ ♆ ♀ ♁ ♐ ♇ ♈ ♉ ♚ ♛ ♜ ♝ ♞ ♟ ♠ ♣ ⚢ ⚲ ⚳ ⚴ ⚥ ⚤ ⚦ ⚒ ⚑ ⚐ ♺ ♻ ♼ ☰ ☱ ☲ ☳ ☴ ☵ ☶ ☷
# ✡ ✔ ✖ ✚ ✱ ✤ ✦ ❤ ➜ ➟ ➼ ✂ ✎ ✐ ⨀ ⨁ ⨂ ⨍ ⨎ ⨏ ⨷ ⩚ ⩛ ⩡ ⩱ ⩲ ⩵  ⩶ ⨠
# ⬅ ⬆ ⬇ ⬈ ⬉ ⬊ ⬋ ⬒ ⬓ ⬔ ⬕ ⬖ ⬗ ⬘ ⬙ ⬟  ⬤ 〒 ǀ ǁ ǂ ĭ Ť Ŧ

prompt_separator() {
    echo " :: "
}

## TODO: rewrite following two functions with using more zsh'ish functions and commands
reduce_pwd() {
    ## $1 - pwd
    # get first part of path
    local PWD=$1
    local str
    local path_array
    path_array=( ${(s./.)PWD} )
    for i in $(seq ${#path_array}); do
        if [[ ${#path_array[$i]} -gt 1 ]]; then 
            str=${path_array[$i]}
            path_array[$i]=${str:0:1}
            break
        fi
    done
    echo "${(j./.)path_array}"
}

dir_name() {
    local PWD=$(pwd)
    if [[ $PWD =~ $HOME ]]; then
        PWD=$(echo "${PWD/$HOME/~}")
    fi
    if [[ $PWD != "/" ]]; then
        PWD="${PWD}/"
    fi
    while [[ ${#PWD} -ge $MAXPWD ]]; do
        local last_part=$(basename $PWD)
        local rest_part=$(dirname $PWD)
        local new_pwd="$(reduce_pwd $rest_part)/${last_part}"
        if [[ $new_pwd == $PWD ]]; then
            break
        else
            PWD=$new_pwd
        fi
    done
    echo "%F{$blue}$PWD %f"
    # echo "%F{$blue}%~%(1/./.) %f"
}
## Dynamic named directories
## see more in "man zshexpn" section 'FILENAME EXPANSION'
# zsh_directory_name() {
#     emulate -L zsh
#     setopt extendedglob
#     local -a match mbegin mend
#     if [[ $1 = d ]]; then
#         # turn the directory into a name
#         if [[ $2 = (#b)(/home/pws/perforce/)([^/]##)* ]]; then
#         typeset -ga reply
#         reply=(p:$match[2] $(( ${#match[1]} + ${#match[2]} )) )
#         else
#         return 1
#         fi
#     elif [[ $1 = n ]]; then
#         # turn the name into a directory
#         [[ $2 != (#b)p:(?*) ]] && return 1
#         typeset -ga reply
#         reply=(/home/pws/perforce/$match[1])
#     elif [[ $1 = c ]]; then
#         # complete names
#         local expl
#         local -a dirs
#         dirs=(/home/pws/perforce/*(/:t))
#         dirs=(p:${^dirs})
#         _wanted dynamic-dirs expl 'dynamic directory' compadd -S\] -a dirs
#         return
#     else
#         return 1
#     fi
#     return 0
# }

user_name() {
    if is_root; then
        echo "%F{$red}%n%f"
    else
        echo "%F{$green}%n%f"
    fi
}

box_name() {
    if is_ssh; then
        [[ -f ~/.box-name ]] && echo "%F{$yellow}$(cat ~/.box-name) %f" || echo "%F{$yellow}%2m %f"
    else
        [[ -f ~/.box-name ]] && echo "%F{$green}$(cat ~/.box-name) %f" || echo "%F{$green}%m %f"
    fi
}

return_code_simbol() {
    if has_powerline; then
        echo "%(?..%F{$red}✘ %f)"
    ## TODO: else show simple simbol
    fi
}

return_code_number() {
    echo "%(?..%F{$red}%? ↵%f)"
}

bg_jobs() {
    if has_powerline; then
        [[ $(jobs -l | wc -l) -gt 0 ]] && echo "%F{$cyan}⚙ %f"
    ## TODO: else show simple simbol
    fi
}

prompt_sign() {
    if is_ssh; then
        echo "%(!.%F{$red}⨀.%F{$blue}») %f"
    else
        echo "%(!.%F{$red}○.%F{$blue}❯) %f"
    fi
}

date_time() {
    echo " %F{$grey}‹%D{%F}$(prompt_separator)%*›%f"
}

## vcs_info's hooks
+vi-extened-vcs-message(){
    if [[ ${hook_com[vcs_orig]} == git ]]; then
        hook_com[vcs]=${GIT_ICON}
        ## Untracked files
        git status --porcelain | grep -q '??' && hook_com[unstaged]="%F{$yellow}✭ %f${hook_com[unstaged]}"
    elif [[ ${hook_com[vcs_orig]} == hg ]]; then
        hook_com[vcs]=${HG_ICON}
        ## Untracked files
        hg status --unknown | grep -q '?' && hook_com[unstaged]="%F{$yellow}✭ %f${hook_com[unstaged]}"
    fi
}

zstyle ':vcs_info:*' enable git hg cvs
# zstyle ':vcs_info:*+*:*' debug true
zstyle ':vcs_info:*' get-revision true
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*+set-message:*' hooks extened-vcs-message
zstyle ':vcs_info:*' formats "%F{$orange}%s%f %0.7i@%F{$magenta}%b%f %u%c"
zstyle ':vcs_info:*' actionformats "%F{$orange}%s%f %0.7i@%F{$magenta}%b%f:%a %u%c"
zstyle ':vcs_info:*' branchformat '%b'
zstyle ':vcs_info:*' hgrevformat '%r'
zstyle ':vcs_info:*' stagedstr "%F{$green}± %f"
zstyle ':vcs_info:*' unstagedstr "%F{$cyan}● %f"

add-zsh-hook precmd vcs_info

PROMPT='$(return_code_simbol)$(bg_jobs)$(user_name)@$(box_name)${vcs_info_msg_0_}
$(dir_name)$(prompt_sign)'

if is_my_tmux; then
    RPROMPT='$(return_code_number)'
else
    RPROMPT='$(return_code_number)$(date_time)'
fi
