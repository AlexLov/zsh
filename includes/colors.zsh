has_colors && autoload colors && colors

if has_256colors; then
    blue="032"
    grey="239"
    green="118"
    yellow="226"
    red="161"
    cyan="081"
    magenta="135"
    orange="166"
elif has_colors; then 
    blue="blue"
    grey="blue"
    green="green"
    yellow="yellow"
    red="red"
    cyan="cyan"
    magenta="magenta"
    orange="yellow"
else
    blue="default"
    grey="default"
    green="default"
    yellow="default"
    red="default"
    cyan="default"
    magenta="default"
    orange="default"
fi

if is_mac || is_freebsd; then
    # Clear LSCOLORS
    unset LSCOLORS

    # Main change, you can see directories on a dark background
    #export LSCOLORS=gxfxcxdxbxegedabagacad
    export CLICOLOR=1
    export LSCOLORS=exfxcxdxbxegedabagacad
elif is_linux; then
    eval $(dircolors)
fi
