autoload -U zsh/terminfo
# Checks (stolen from zshuery and extended)
is_mac() { [[ $OSTYPE == darwin* ]] }
is_freebsd() { [[ $OSTYPE == freebsd* ]] }
is_linux() { [[ $OSTYPE == linux-gnu* ]] }

is_root() { [[ $UID -eq 0 ]] }
## FIXME: find alternate way to check that is is ssh connection
##        ($SSH_CONNECTION is not available after sudo)
## There is a HACK for sudo env, just add following to sudoers file
## 'Defaults env_keep += "SSH_CONNECTION"' 
is_ssh() { [[ -n $SSH_CONNECTION ]] }
## FIXME: find a better way to determine tmux
is_tmux() { [[ $TERM == screen* ]] }
is_my_tmux() { is_tmux && [[ -n $MY_TMUX ]] }
is_screen() { [[ $TERM == screen* ]] }
is_sudo() { [[ -n $SUDO_USER ]] }

has_brew() { [[ -n ${commands[brew]} ]] }
has_apt() { [[ -n ${commands[apt-get]} ]] }
has_yum() { [[ -n ${commands[yum]} ]] }
has_pacman() { [[ -n ${commands[pacman]} ]] }
has_git() { [[ -n ${commands[git]} ]] }
has_hg() { [[ -n ${commands[hg]} ]] }
has_tmux() { [[ -n ${commands[tmux]} ]] }
has_vagrant() { [[ -n ${commands[vagrant]} ]] }
has_go() { [[ -n ${commands[go]} ]] }
has_colordiff() { [[ -n ${commands[colordiff]} ]] }
has_mtr() { [[ -n ${commands[mtr]} ]] }
has_mosh() { [[ -n ${commands[mosh]} ]] }
has_taskwarrior() { [[ -n ${commands[task]} ]] }
has_timewarrior() { [[ -n ${commands[timew]} ]] }
has_watson() { [[ -n ${commands[watson]} ]] }

has_colors() { [[ ${terminfo[colors]} -ge 8 ]] }
has_256colors() { [[ ${terminfo[colors]} -ge 256 ]] }

## placeholders
is_utf8() { true }
is_my_screen() { is_screen }
## indirect check
has_powerline() { true }
