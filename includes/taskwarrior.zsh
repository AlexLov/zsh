# -----------------------------------------------------------------------------
# Return uuid of first task in a list sorted by given sort key.
# The rest of options determined as filter.
# Default sort key is 'urgency-'.
# -----------------------------------------------------------------------------
_get_next_task() { # $1 - sort key, $@ - filter
    local sort_key=${1:-"urgency-"}
    local filter=
    local task_uuid=
    if [[ $# -gt 1 ]]; then
        shift
        filter=${@:-}
    fi
    task_uuid=$(task \
        rc.verbose=nothing \
        rc.hooks=off \
        rc.detection=on \
        rc.reserved.lines=0 \
        rc.report.temp.sort="$sort_key" \
        rc.report.temp.columns=uuid \
        rc.report.temp.filter="status:pending limit:1 ${filter}" \
        temp)
    printf '%s' $task_uuid
}

# -----------------------------------------------------------------------------
# Start next task by urgency. If there is no currently active task.
# If there is active current task return it.
# -----------------------------------------------------------------------------
tn() { # $@ - filter
    local filter=${@:-}
    local next_task_uuid=$(_get_next_task '' $filter)
    local active_task_uuid=$(_get_next_task '' +ACTIVE)
    if [[ -n $active_task_uuid ]]; then
        echo "There is already task in progress...\n"
        task $active_task_uuid rc.verbose=label next
    else
        task $next_task_uuid rc.verbose=blank start && \
            task $next_task_uuid rc.verbose=label next
    fi
}

# -----------------------------------------------------------------------------
# Stop current active task and mark it as done. Do nothing if there is no active
# task.
# -----------------------------------------------------------------------------
td() {
    local active_task_uuid=$(_get_next_task '' +ACTIVE)
    if [[ -n $active_task_uuid ]]; then
        local task_id=$(task _get ${active_task_uuid}.id)
        local task_description="$(task _get ${active_task_uuid}.description)"
        task $active_task_uuid rc.verbose=nothing stop && \
        task $active_task_uuid rc.verbose=nothing done && \
        echo "Complited task ${task_id} '${task_description}'"
    else
        echo "There is no active task"
    fi
}

# -----------------------------------------------------------------------------
# Mark current active task as done and start next one by urgency.
# If there is no active task then just start next one.
# -----------------------------------------------------------------------------
tdn() { # $@ - filter
    local filter=${@:-}
    td && tn '' $filter
}

# -----------------------------------------------------------------------------
# Stop/pause current active task.
# -----------------------------------------------------------------------------
tp() {
    local active_task_uuid=$(_get_next_task '' +ACTIVE)
    if [[ -n $active_task_uuid ]]; then
        local task_id=$(task _get ${active_task_uuid}.id)
        local task_description="$(task _get ${active_task_uuid}.description)"
        task $active_task_uuid rc.verbose=nothing stop && \
        echo "Stopped task ${task_id} '${task_description}'"
    else
        echo "There is no active task"
    fi
}
