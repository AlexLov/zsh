if [[ -n $ZDOTDIR ]]; then
    ZSH_INCLUDES="${ZDOTDIR}/includes"
    ZSH_COMPLETIONS="${ZDOTDIR}/completions"
    ZSH_PRIVATE="${ZDOTDIR}/private"
    ZSH_CUSTOM="${HOME}/.zsh_custom"
elif [[ -h "${HOME}/.zshrc" ]]; then # is symlink?
    zmodload -F zsh/stat b:zstat
    ZSHRC_LINK="$(zstat -L +link ${HOME}/.zshrc)"
    if [[ "${ZSHRC_LINK:0:1}" != "/" ]]; then # this is relative path
        ZSHRC_LINK="$(dirname ${HOME}/.zshrc)/${ZSHRC_LINK}"
    fi
    ZSH_INCLUDES="$(dirname $ZSHRC_LINK)/includes"
    ZSH_COMPLETIONS="$(dirname $ZSHRC_LINK)/completions"
    ZSH_PRIVATE="$(dirname $ZSHRC_LINK)/private"
fi

ZSH_CUSTOM="${HOME}/.zsh_custom"

[[ -d "$ZSH_COMPLETIONS" ]] && fpath=(${ZSH_COMPLETIONS} $fpath)

if [[ -d "$ZSH_INCLUDES" ]]; then
    source "${ZSH_INCLUDES}/checks.zsh"
    source "${ZSH_INCLUDES}/colors.zsh"
    source "${ZSH_INCLUDES}/setopt.zsh"
    source "${ZSH_INCLUDES}/exports.zsh"
    source "${ZSH_INCLUDES}/prompt.zsh"
    source "${ZSH_INCLUDES}/completion.zsh"
    source "${ZSH_INCLUDES}/aliases.zsh"
    source "${ZSH_INCLUDES}/bindkeys.zsh"
    source "${ZSH_INCLUDES}/functions.zsh"
    source "${ZSH_INCLUDES}/history.zsh"
    source "${ZSH_INCLUDES}/termsupport.zsh"
    source "${ZSH_INCLUDES}/zsh_hooks.zsh"
    source "${ZSH_INCLUDES}/syntax-highlighting-filetypes.zsh"
    source "${ZSH_INCLUDES}/zsh-autosuggestions.zsh"
    ## If not found iterm2 shell integration
    if ! [[ -s "${ZSH_INCLUDES}/iterm2_shell_integration.zsh" ]]; then
        ## try to download it
        curl -SsL 'https://iterm2.com/misc/zsh_startup.in' > "${ZSH_INCLUDES}/iterm2_shell_integration.zsh"
    fi
    test -s "${ZSH_INCLUDES}/iterm2_shell_integration.zsh" && source "${ZSH_INCLUDES}/iterm2_shell_integration.zsh"
    has_taskwarrior && source "${ZSH_INCLUDES}/taskwarrior.zsh"
fi

## Include private zsh files
if [[ -d "$ZSH_PRIVATE" ]]; then
    for file in ${ZSH_PRIVATE}/*.zsh; do
        source $file
    done
fi

## Include custom zsh file
[[ -f "$ZSH_CUSTOM" ]] && source $ZSH_CUSTOM

